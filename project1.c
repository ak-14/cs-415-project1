/*
 * Abdulrahman Alkhelaifi
 * CS 415 - Project1
 * April 26, 2013
 */

#include <unistd.h>
#include <signal.h>
#include <stdlib.h>

#define BUFF_SIZE 1024
#define ARGS_LENGTH 32

#define PROMPT "Wile E.# "
#define SUCCESS_MSG "Meep Meep!\n"
#define FAIL_MSG "Wile E. Coyote, Supergenius!\n"
#define ARGS_ERROR "error: Wile E. can't handle too many arguments!\n"

pid_t pid;

// sigalarm handler
void handler (int signum) {
	kill(pid, SIGKILL);
}

int my_atoi(char *s) {
	int i, j, sum;
	for (i = 0; s[i] != '\0'; i++); //length of s
	i--;
	sum = 0;
	for (j = 1; i >= 0; j *= 10, i--) {
		sum += (s[i] - 48) * j;
	}
	return sum;
}

int my_strcmp(char *s1, char *s2) {
	int i;
	for (i = 0; s1[i] == s2[i]; i++) {
		if (s1[i] == '\0' || s2[i] == '\0')
			break;
	}
	if (s1[i] == '\0' && s2[i] == '\0')
		return 0;
	return -1;
}

int main(int argc, char **argv) {

	int readRet, writeRet;
	int waitTime, status;
	int	index, i;	
	char buffer[BUFF_SIZE];
	char *args[ARGS_LENGTH]; //predefined arguments length
	char **envp;

	//set max wiat time
	if (argc > 1)
		waitTime = my_atoi(argv[1]);
	else
		waitTime = -1;

	signal(SIGALRM, handler);

	while (1) {
		writeRet = write(1, PROMPT, sizeof(PROMPT));

		if (writeRet == -1)
			perror(__func__);

		readRet = read(0, buffer, sizeof(buffer));

		if (readRet == -1)
			perror(__func__);

		buffer[readRet-1] = '\0'; //null terminate

		if (my_strcmp(buffer,"exit") == 0)
			exit(0);

		//build args array
		args[0] = &buffer[0];
		index = 1;

		for (i = 0; i < readRet && index < 32; i++) {
			if (buffer[i] == ' ') {
				buffer[i] = '\0';
				args[index] = &buffer[i+1];
				index++;
			}
		}
		if (index >= 31) {
			write(1, ARGS_ERROR, sizeof(ARGS_ERROR));
			continue; 
		}

		args[index] = NULL; //NULL terminate
		envp = NULL;

		pid = fork();

		if (!pid) { //CHILD
			execve(buffer, args, envp);

			//get here only if execve returns
			perror(__func__);
			exit(EXIT_FAILURE);

		} else { //PARENT
			if (waitTime != -1)
				alarm(waitTime);

			//check wait return value
			if (wait(&status) == -1)
				perror(__func__);

			alarm(0); //reset alarm

			//check if child exited or was killed
			if (WIFSIGNALED(status) && WTERMSIG(status) == SIGKILL) {
				writeRet = write(1, FAIL_MSG, sizeof(FAIL_MSG));

			} else if (WIFEXITED(status) && WEXITSTATUS(status) == EXIT_SUCCESS) {
				writeRet = write(1, SUCCESS_MSG, sizeof(SUCCESS_MSG));

			} 
			//check write return value
			if (writeRet == -1)
				perror(__func__);
		}
	}
}
