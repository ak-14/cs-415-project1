CC=gcc

DESTDIR=/tmp

project1: project1.c
	$(CC) -o project1 project1.c -I.

install:
	install -d $(DESTDIR)
	install -m 0755 project1 $(DESTDIR)

clean:
	rm project1 /tmp/project1
